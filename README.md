# RCPFASC

## ¿Qué es RCPFASC?

RCPFASC es un programa que apareció en Analog Dialogue Vol. 10, No. 1 (1976), “Simple Rules for Choosing Resistor Values in Adder-Subtractor Circuits” 	por Dan Sheingold, escrito en BASIC que trascribí para C. Este programa es muy pequeñito, aun así lo libero como software libre.


![ALT](ampopaddsustr.jpg)
