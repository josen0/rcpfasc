/*
	RCPFASC v1.0

	Based in text and code
	Analog Dialogue Vol. 10, No. 1 (1976), �Simple Rules for Choosing Resistor Values in Adder-Subtractor Circuits�
	HP-25 RESISTANCE-COMPUTATION PROGRAM FOR ADDER-SUBSTRACTOR CIRCUITS
	By Dan Sheingold

	Copyright (C) 2018 J. Nieto, all rights reserved.
	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
   	the Free Software Foundation; either version 3, or (at your option)
   	any later version.

   	This program is distributed in the hope that it will be useful,
   	but WITHOUT ANY WARRANTY; without even the implied warranty of
   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   	GNU General Public License for more details.

 	You should have received a copy of the GNU General Public License
    	along with this program.  If not, see <https://www.gnu.org/licenses/>.

	contact: kkeezff@hotmail.com

*/

#pragma hdrstop
#pragma argsused

#ifdef _WIN32
#include <tchar.h>
#else
  typedef char _TCHAR;
  #define _tmain main
#endif
#include<iostream>
#include<iomanip>
#include<cmath>

int _tmain(int argc, _TCHAR* argv[])
{
float larger_sum;
float delta;
float sum_a, sum_b;
float a1,a2,a3;
float b1,b2,b3;
float Rp, Rf, Rp_fbl, Rp_ao;
std::cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
std::cout << "++ RESISTANCE-COMPUTATION PROGRAM FOR ADDER-SUBSTRACTOR CIRCUITS ++" << std::endl;
std::cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;

	std::cout << "Magnitude positive coefficient a1 (for no coefficient, put 0): ";
	std::cin >> a1;
	std::cout << "Magnitude positive coefficient a2 (for no coefficient, put 0): ";
	std::cin >> a2;
	std::cout << "Magnitude positive coefficient a3 (for no coefficient, put 0): ";
	std::cin >> a3;
	std::cout << "Magnitude negative coefficient b1 (for no coefficient, put 0): ";
	std::cin >> b1;
	std::cout << "Magnitude negative coefficient b2 (for no coefficient, put 0): ";
	std::cin >> b2;
	std::cout << "Magnitude negative coefficient b3 (for no coefficient, put 0): ";
	std::cin >> b3;

	std::cout << "a1 = " << a1 << " a2 = " << a2 << " a3 = " << a3 << std::endl;
	std::cout << "b1 = " << b1 << " b2 = " << b2 << " b3 = " << b3 << std::endl;

	sum_a = a1+a2+a3;
	sum_b = b1+b2+b3+1;

	if(sum_b > sum_a)
		larger_sum = sum_b;
	else
		larger_sum = sum_a;

	delta = sum_b - sum_a;

	std::cout << "Value for effective parallel resistance (Rp): ";

	std::cin >> Rp;

	std::cout << "Rp = " << Rp << std::endl;

	Rf = Rp*larger_sum;

	std::cout << "Rf = " << Rf << std::endl;

	Rp_fbl = 0.0;
	Rp_ao = 0.0;

	Rp_fbl += 1/Rf;




	if(delta < 0.0){
		std::cout << "RL = " << std::fixed << std::setprecision(3) << -Rf/delta << std::endl;

		Rp_fbl += -delta/Rf;
	}
	if(delta > 0.0){
		std::cout << "RO = " << std::fixed << std::setprecision(3) << Rf/delta << std::endl;
		Rp_ao += delta/Rf;
	}
	if(delta == 0.0)
		std::cout << "No RO or RL" << std::endl;

	if(a1 != 0.0){
		std::cout << "Ra1 = " << Rf/a1 << std::endl;
		Rp_ao += a1/Rf;
	}
	if(a2 != 0.0){
		std::cout << "Ra2 = " << Rf/a2 << std::endl;
		Rp_ao += a2/Rf;
	}
	if(a3 != 0.0){
		std::cout << "Ra3 = " << Rf/a3 << std::endl;
		Rp_ao += a3/Rf;
	}
	if(b1 != 0.0){
		std::cout << "Rb1 = " << Rf/b1 << std::endl;
		Rp_fbl += b1/Rf;
	}
	if(b2 != 0.0){
		std::cout << "Rb2 = " << Rf/b2 << std::endl;
		Rp_fbl += b2/Rf;
	}
	if(b3 != 0.0){
		std::cout << "Rb3 = " << Rf/b3 << std::endl;
		Rp_fbl += b3/Rf;
	}

	if(Rp_ao > 0 && Rp_fbl > 0){
		Rp_ao = std::pow(Rp_ao,-1);
		Rp_fbl = std::pow(Rp_fbl,-1);
	}

	std::cout << "Rp\t=\tRf||Rb1||Rb2||Rb3||RL\t=\tRa1||Ra2||Ra3||RO" << std::endl;
	std::cout << Rp << "\t=\t" << Rp_fbl << "\t=\t" << Rp_ao << std::endl;

	std::cout << "Bye!" << std::endl;

	return 0;
}
